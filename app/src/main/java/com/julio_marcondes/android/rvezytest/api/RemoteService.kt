package com.julio_marcondes.android.rvezytest.api

import com.julio_marcondes.android.rvezytest.AuthInterceptor
import com.julio_marcondes.android.rvezytest.data.Photos
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Used to connect to the RemoteService API to fetch data in remote server
 */
interface RemoteService {

    @GET("v1/images/search?order=Desc")
    suspend fun getImages(
        @Query ("limit") randomLimit: Int = (1..50).random(),
        @Query ("page") pageNumber: Int = 0
    ): List<Photos>

    companion object {
        private const val BASE_URL = "https://api.thecatapi.com/"

        fun create(): RemoteService {
            val client = OkHttpClient.Builder()
                .addInterceptor(AuthInterceptor())
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RemoteService::class.java)
        }
    }
}