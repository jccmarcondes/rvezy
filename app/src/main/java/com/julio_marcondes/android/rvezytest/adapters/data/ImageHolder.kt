package com.julio_marcondes.android.rvezytest.adapters.data

import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import com.julio_marcondes.android.rvezytest.R
import com.julio_marcondes.android.rvezytest.data.Photos
import com.julio_marcondes.android.rvezytest.dialog.CustomDialog
import com.julio_marcondes.android.rvezytest.utils.loadFromURL

class ImageHolder(view: View) : DataViewHolder<Photos>(view) {
    private val imageContent = view.findViewById<ImageView>(R.id.iv_image)

    override fun bind(item: Photos) {
        item.run {
            imageContent.run {
                loadFromURL(url)
                setOnClickListener {
                    var localLayoutParams = WindowManager.LayoutParams()
                    localLayoutParams.width = item.width
                    localLayoutParams.height = item.height

                    CustomDialog(this.context).run {
                        if (breeds.isNotEmpty()) {
                            localLayoutParams.height = item.height.plus(breeds.size * 4)
                            buildDialog(url, breeds, localLayoutParams)
                        } else {
                            localLayoutParams.height = item.height
                            buildDialog(url, null, localLayoutParams)
                        }
                        show()
                    }
                }
            }
        }
    }

    companion object {
        const val FIRST_BREED = 0
    }

}