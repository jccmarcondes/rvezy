package com.julio_marcondes.android.rvezytest.inject

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.julio_marcondes.android.rvezytest.viewmodel.DataViewModel
import com.julio_marcondes.android.rvezytest.viewmodel.ViewModelFactory
import com.julio_marcondes.android.rvezytest.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(DataViewModel::class)
    internal abstract fun bindDataViewModel(viewModel: DataViewModel): ViewModel
}