package com.julio_marcondes.android.rvezytest.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.julio_marcondes.android.rvezytest.R
import com.julio_marcondes.android.rvezytest.utils.loadFromURL

class CustomDialog(
    context: Context
) : AlertDialog(context) {

    fun buildDialog(imagePath: String, breed: List<String>?, parameters: WindowManager.LayoutParams) {
        val view = View.inflate(context, R.layout.item_image_grid, null)

        view.apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window?.attributes = parameters

            findViewById<ImageView>(R.id.iv_image)?.run {
                loadFromURL(imagePath)
            }

            breed?.let {
                findViewById<TextView>(R.id.tv_breed)?.run {
                    isVisible = true
                    var finalBreeds = StringBuilder("Breeds:\n")
                    it.forEach {
                        finalBreeds.append("- ").append(it).append("\n")
                    }
                    text = finalBreeds.toString()
                }
            }
        }
        setView(view)
    }
}