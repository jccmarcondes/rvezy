package com.julio_marcondes.android.rvezytest.inject

import android.app.Application
import com.julio_marcondes.android.rvezytest.MainActivity
import com.julio_marcondes.android.rvezytest.view.DataFragment
import dagger.BindsInstance
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelModule::class])
interface RVezyComponent {

    fun inject(activity: MainActivity)
    fun inject(fragment: DataFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance application: Application,
        ): RVezyComponent
    }

}