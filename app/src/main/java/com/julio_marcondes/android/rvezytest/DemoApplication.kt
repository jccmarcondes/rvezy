package com.julio_marcondes.android.rvezytest

import android.app.Application

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        RVezyModule.init(
            application = this
        )
    }

}