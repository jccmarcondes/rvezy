package com.julio_marcondes.android.rvezytest.domain.repository

class ImageItemUi(val breeds: List<String>, val id: String = "", val url: String? = null,
                 val width: Int = 0, val height: Int = 0)