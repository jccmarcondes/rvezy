package com.julio_marcondes.android.rvezytest.usecase

import com.julio_marcondes.android.rvezytest.domain.repository.DataRepository
import com.julio_marcondes.android.rvezytest.utils.safeRunDispatcher
import javax.inject.Inject

class PhotoDataUseCase @Inject constructor(private val repository: DataRepository) {

    suspend operator fun invoke(pageNumber: Int) =
        safeRunDispatcher {
            repository.getImages(pageNumber)
        }
}