package com.julio_marcondes.android.rvezytest.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.julio_marcondes.android.rvezytest.domain.repository.DataRepository
import com.julio_marcondes.android.rvezytest.usecase.PhotoDataUseCase

/**
 * Factory for creating a [DataViewModel] with a constructor that takes a [DataRepository].
 */
class DataViewModelFactory(
    private val useCase: PhotoDataUseCase,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return DataViewModel(useCase) as T
    }
}