package com.julio_marcondes.android.rvezytest.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.julio_marcondes.android.rvezytest.RVezyModule
import com.julio_marcondes.android.rvezytest.adapters.PhotoAdapter
import com.julio_marcondes.android.rvezytest.base.BaseBindingFragment
import com.julio_marcondes.android.rvezytest.databinding.FragmentDataBinding
import com.julio_marcondes.android.rvezytest.viewmodel.DataViewModel

class DataFragment : BaseBindingFragment<FragmentDataBinding>() {

    private val viewModel: DataViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[DataViewModel::class.java]
    }

    override fun injectDependencies() = RVezyModule.component.inject(this)

    override fun binding(inflater: LayoutInflater) = FragmentDataBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservables()
        setupLastButton()
        setupNextButton()
        viewModel.actualPage.value = FIRST_PAGE
        viewModel.setup()
    }

    private fun setupObservables() {
        viewModel.photoList.observe(viewLifecycleOwner) {
            binding.rvPhotoGrid.adapter = PhotoAdapter(it)
        }
    }

    private fun setupLastButton() {
        binding.btLast.setOnClickListener { viewModel.requestLastPage() }
    }

    private fun setupNextButton() {
        binding.btNext.setOnClickListener { viewModel.requestNextPage() }
    }

    companion object {
        const val FIRST_PAGE = 0
    }
}