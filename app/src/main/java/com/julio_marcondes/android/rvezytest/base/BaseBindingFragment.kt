package com.julio_marcondes.android.rvezytest.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.julio_marcondes.android.rvezytest.viewmodel.ViewModelFactory
import javax.inject.Inject

abstract class BaseBindingFragment<V : ViewBinding> : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    protected lateinit var binding: V

    protected abstract fun injectDependencies()

    protected abstract fun binding(inflater: LayoutInflater): V

    @CallSuper
    override fun onAttach(context: Context) {
        injectDependencies()
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = binding(inflater)
        return binding.root
    }
}