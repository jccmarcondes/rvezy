package com.julio_marcondes.android.rvezytest

import android.os.Bundle
import com.julio_marcondes.android.rvezytest.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}