package com.julio_marcondes.android.rvezytest.domain.repository

import com.julio_marcondes.android.rvezytest.api.RemoteService
import com.julio_marcondes.android.rvezytest.data.Photos
import javax.inject.Inject


class DataRepository @Inject constructor() {

    suspend fun getImages(pageNumber: Int): List<Photos> = RemoteService.create().getImages(pageNumber = pageNumber)

}