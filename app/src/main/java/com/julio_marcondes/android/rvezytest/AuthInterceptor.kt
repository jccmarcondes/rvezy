package com.julio_marcondes.android.rvezytest

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import javax.inject.Singleton

@Singleton
class AuthInterceptor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val response: Response

        val builder = request.newBuilder()
        builder.run {
            this.addHeader(KEY_X_API_KEY, "24be637f-e596-4847-b47a-1791feeea1bd")
            this.addHeader(KEY_ACCEPT, KEY_ACCEPT_VALUE)
        }


        request = builder.build()

        response = chain.proceed(request)
        val body = response.body
        val bodyString = body?.string() ?: ""
        val contentType = body?.contentType()
        return response.newBuilder()
            .body(ResponseBody.create(contentType, bodyString))
            .build()
    }

    companion object {
        private const val KEY_X_API_KEY = "x-api-key"
        private const val KEY_ACCEPT = "accept"
        private const val KEY_ACCEPT_VALUE = "application/json"
    }
}