package com.julio_marcondes.android.rvezytest

import android.app.Application
import com.julio_marcondes.android.rvezytest.inject.DaggerRVezyComponent
import com.julio_marcondes.android.rvezytest.inject.RVezyComponent

object RVezyModule {

    lateinit var component: RVezyComponent
    lateinit var rVezyApplication: Application

    fun init(
        application: Application
    ) {
        rVezyApplication = application
        component = DaggerRVezyComponent
            .factory()
            .create(application = application)
    }

}