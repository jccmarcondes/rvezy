package com.julio_marcondes.android.rvezytest.adapters.data

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class DataViewHolder<in T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T)
}