package com.julio_marcondes.android.rvezytest.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.julio_marcondes.android.rvezytest.data.Photos
import com.julio_marcondes.android.rvezytest.utils.Result
import com.julio_marcondes.android.rvezytest.usecase.PhotoDataUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * The ViewModel for [DataFragment].
 */
class DataViewModel @Inject constructor(
    private val usecase: PhotoDataUseCase
) : ViewModel() {

    var photoList = MutableLiveData<MutableList<Photos>>()
    var actualPage = MutableLiveData<Int>()

    fun setup() = viewModelScope.launch {
        val result = usecase.invoke(actualPage.value!!)

        when (result) {
            is Result.Success<*> -> {
                photoList.value = (result.data as MutableList<Photos>)
                Log.d(TAG, "Data received with success !")
            }
            is Result.Failure -> {
                Log.e(TAG, "Error receiving data from server !")
            }
        }
    }

    fun requestLastPage() {
        actualPage.value?.let {
            if (it > 0) actualPage.value = it.minus(1)
            setup()
        }
    }

    fun requestNextPage() {
        actualPage.value?.let {
            actualPage.value = it.plus(1)
            setup()
        }
    }

    companion object {
        const val TAG = "DataViewModel"
    }
}