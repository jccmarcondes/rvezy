package com.julio_marcondes.android.rvezytest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.julio_marcondes.android.rvezytest.R
import com.julio_marcondes.android.rvezytest.adapters.data.ImageHolder
import com.julio_marcondes.android.rvezytest.data.Photos

/**
 * Adapter for the [RecyclerView] in [DataFragment].
 */
class PhotoAdapter(var data: MutableList<Photos>) : RecyclerView.Adapter<ImageHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_image_grid, parent, false)
        return ImageHolder(view)
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size
}

