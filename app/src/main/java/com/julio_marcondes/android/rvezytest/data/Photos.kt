package com.julio_marcondes.android.rvezytest.data

import com.google.gson.annotations.SerializedName

data class Photos (
   @SerializedName("breeds") val breeds: List<String>,
   @SerializedName("id") val id: String = "",
   @SerializedName("url") val url: String = "",
   @SerializedName("width") val width: Int = 0,
   @SerializedName("height") val height: Int = 0
)